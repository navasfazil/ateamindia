<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ateamindia' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j-]/OcV=-A!|B_M_=%/E`5$TOoWlM!8,<_b idn2W<<{<?NxpE/qz!9)h)Ejfwp@' );
define( 'SECURE_AUTH_KEY',  '<b.2zR>B=7%j_kGva7$rU0oG82h|UFmoMK$SLwj|YXAO-N 36Uq-rEw9&km`17$U' );
define( 'LOGGED_IN_KEY',    '5&wAaN~f;%VX5R-$T(_c*i87`&@!0fM*cM[Z_yP0>}tRCpDXWufrj5!F~Gs.8byG' );
define( 'NONCE_KEY',        ']$L+N7ZBq5z1xd~P/yZe<Agv}&}Xb,^{i@W ;ISX6jzL &>3-PGc9s?_N(;%OHbZ' );
define( 'AUTH_SALT',        'UUqqHcc`Ngu=d3oPIEal)rTSJT)&d|H4?3WuB;IJC)C%JDH@2*>d:-,~]$[y_c&?' );
define( 'SECURE_AUTH_SALT', 'CFKl&cEV<ekGllO>AQq{G,~7VtsX8zGT,$70-h<Nw*.rMSjk8#e4gj<6A(5uF1(:' );
define( 'LOGGED_IN_SALT',   'LYP(3L&77f`gI^<Up?OA+WXv4V<!qiuy<|WCYk6:n]Kn)teZ-MBkaFus}8i@wS*s' );
define( 'NONCE_SALT',       '`o~97lp6{ky@{_:~A$xUQvF%.5/yCnQ$oJ^}K.>=eR!@t@r;lfY{tO5YxwQ _Eh#' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ati_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
